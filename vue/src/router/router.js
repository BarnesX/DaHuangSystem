import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: { path: '/file', query: { filepath: '/', filetype: 0 } }
    }, {
      path: '/login',
      name: 'Login',
      component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
      meta: { title: '登录 - 大黄管理系统' },
    }, {
      path: '/register',
      name: 'Register',
      component: () => import(/* webpackChunkName: "register" */ '@/views/Register.vue'),
      meta: { title: '注册 - 大黄管理系统' },
    }, {
      path: '/file',
      name: 'File',
      component: () => import(/* webpackChunkName: "file" */ '@/views/file/File.vue'),
      meta: {
        requireAuth: true,
        title: '大黄多媒体资源管理系统',
        content: {
          description: '多媒体资源文件的存储平台'
        }
      }
    }, {
      path: '/500',
      name: 'Error_500',
      component: () => import(/* webpackChunkName: "error_500" */ '@/views/ErrorPage/500.vue'),
      meta: { title: '500 - 大黄管理系统' },
    }, {
      path: '/401',
      name: 'Error_401',
      component: () => import(/* webpackChunkName: "error_401" */ '@/views/ErrorPage/401.vue'),
      meta: { title: '401 - 大黄管理系统' },
    }, {
      path: '*',
      name: 'Error_404',
      component: () => import(/* webpackChunkName: "error_404" */ '@/views/ErrorPage/404.vue'),
      meta: { title: '404 - 大黄管理系统' },
    }
  ]
})


const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
};
